#!/bin/bash
#SBATCH --time=7-00:00:00
#SBATCH --nodes=1
#SBATCH --ntasks=5
#SBATCH --partition=gelifes
#SBATCH --job-name=m1
#SBATCH --output=models/M1/m1.out
#SBATCH --mem=10G

module load JAGS/4.3.0-foss-2018a
module load R/4.0.0-foss-2020a 
Rscript models/M1/m1.R

