#!/bin/bash
#SBATCH --time=3-00:00:00
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --partition=regular
#SBATCH --job-name=m10_sa
#SBATCH --output=models/M10/sa10.out
#SBATCH --mem=90G

module load JAGS/4.3.0-foss-2018a
module load R/4.0.0-foss-2020a 
Rscript models/M10/sa10.R

