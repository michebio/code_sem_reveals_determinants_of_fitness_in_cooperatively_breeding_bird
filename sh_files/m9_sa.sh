#!/bin/bash
#SBATCH --time=2-20:00:00
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --partition=gelifes
#SBATCH --job-name=m9_sa
#SBATCH --output=models/M9/sa9.out
#SBATCH --mem=90G

module load JAGS/4.3.0-foss-2018a
module load R/4.0.0-foss-2020a 
Rscript models/M9/sa9.R

