#!/bin/bash
#SBATCH --time=7-00:00:00
#SBATCH --nodes=1
#SBATCH --ntasks=5
#SBATCH --partition=regular
#SBATCH --job-name=m3
#SBATCH --output=models/M3/m1.out
#SBATCH --mem=10G

module load JAGS/4.3.0-foss-2018a
module load R/4.0.0-foss-2020a 
Rscript models/M3/m1.R

