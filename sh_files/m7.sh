#!/bin/bash
#SBATCH --time=2-20:00:00
#SBATCH --nodes=1
#SBATCH --ntasks=5
#SBATCH --partition=gelifes
#SBATCH --job-name=m7
#SBATCH --output=models/M7/m7.out
#SBATCH --mem=10G

module load JAGS/4.3.0-foss-2018a
module load R/4.0.0-foss-2020a 
Rscript models/M7/m7.R

