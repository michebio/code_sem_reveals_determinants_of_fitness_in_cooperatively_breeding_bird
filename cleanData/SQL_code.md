# Get translocated bird to denis and frigate: save as qr_translocated
```
SELECT tblTranslocations.CatchIsland, tblTranslocations.ReleaseIsland, sys_OccasionPlusFieldPeriod.OccasionDate, sys_OccasionPlusFieldPeriod.FieldPeriodID, tblTranslocations.BirdID
FROM sys_OccasionPlusFieldPeriod INNER JOIN (tblCatches INNER JOIN tblTranslocations ON tblCatches.CatchID = tblTranslocations.CatchID) ON sys_OccasionPlusFieldPeriod.OccasionID = tblCatches.OccasionID
WHERE (((tblTranslocations.ReleaseIsland)="DS")) OR (((tblTranslocations.ReleaseIsland)="FR"));
```
Then run and save qr1_mb and save mb_tb0_part0
```
SELECT tblBirdID.BirdID, usys_qBreedStatusMinMaxDatesByIslandInclObsAndCatch.LastObs, Year([LastObs]) AS deathYear, ([LastObs]-[BirthDate])/365.25 AS AgeLastSeen, Round([AgeLastSeen],2) AS deathAge, usys_qBreedStatusMinMaxDatesByIslandInclObsAndCatch.Island, qr_translocated.ReleaseIsland
FROM (tblBirdID INNER JOIN usys_qBreedStatusMinMaxDatesByIslandInclObsAndCatch ON tblBirdID.BirdID = usys_qBreedStatusMinMaxDatesByIslandInclObsAndCatch.BirdID) LEFT JOIN qr_translocated ON tblBirdID.BirdID = qr_translocated.BirdID
WHERE (((tblBirdID.BirdID)>0) AND ((usys_qBreedStatusMinMaxDatesByIslandInclObsAndCatch.LastObs)<#1/1/2014#) AND ((usys_qBreedStatusMinMaxDatesByIslandInclObsAndCatch.Island)="CN"));
Then runa and save as a table mb_tbStatuses and as a query qr_statuses_mb
SELECT tblBreedStatus.BirdID, tblBreedStatus.Status, tblBreedStatus.BreedGroupID, tblBreedGroupLocation.FieldPeriodID, tblFieldPeriodIDs.Island, tblFieldPeriodIDs.PeriodEnd
FROM tblFieldPeriodIDs INNER JOIN (tblBreedGroupLocation INNER JOIN tblBreedStatus ON tblBreedGroupLocation.BreedGroupID = tblBreedStatus.BreedGroupID) ON tblFieldPeriodIDs.FieldPeriodID = tblBreedGroupLocation.FieldPeriodID
WHERE (((tblBreedStatus.BirdID)>0) AND ((tblFieldPeriodIDs.Island)="CN"));
```
Then run and save qr2_mb and save as mb_tb0_part1

```
SELECT DISTINCTROW tblTerritories.TerritoryNumber, mb_tbStatuses.BirdID, mb_tbStatuses.BreedGroupID, mb_tbStatuses.FieldPeriodID, mb_tbStatuses.PeriodEnd, sys_SexEstimates.SexEstimate, mb_tbStatuses.Status
FROM ((tblTerritories INNER JOIN tblBreedGroupLocation ON tblTerritories.TerritoryID = tblBreedGroupLocation.TerritoryID) INNER JOIN mb_tbStatuses ON tblBreedGroupLocation.BreedGroupID = mb_tbStatuses.BreedGroupID) LEFT JOIN sys_SexEstimates ON mb_tbStatuses.BirdID = sys_SexEstimates.BirdID
WHERE (((tblTerritories.Island)="CN"));
```

Then run and save qr3_mb and save as table mb_tb0

```
SELECT mb_tb0_part1.BirdID, mb_tb0_part1.FieldPeriodID, mb_tb0_part1.Status, mb_tb0_part1.BreedGroupID, mb_tb0_part1.PeriodEnd, mb_tb0_part1.TerritoryNumber, mb_tb0_part1.SexEstimate, mb_tb0_part0.LastObs AS LastObsBeforeDeatn, mb_tb0_part0.deathYear, mb_tb0_part0.deathAge, Round(([PeriodEnd]-[BirthDate])/365.25,2) AS age, tblBirdID.BirthDate, mb_tb0_part0.ReleaseIsland
FROM (mb_tb0_part0 RIGHT JOIN mb_tb0_part1 ON mb_tb0_part0.BirdID = mb_tb0_part1.BirdID) LEFT JOIN tblBirdID ON mb_tb0_part1.BirdID = tblBirdID.BirdID
WHERE (((mb_tb0_part1.BirdID)>=0));
```
Run and save as table mb_births

```
SELECT DISTINCT tblBirdID.BirthDate, mb_tb0_part0.BirdID
FROM mb_tb0_part0 LEFT JOIN tblBirdID ON mb_tb0_part0.BirdID = tblBirdID.BirdID
WHERE (((tblBirdID.BirthDate) Is Not Null));
```
Run and save as table mb_birthsAndAge

```
SELECT DISTINCT mb_tb0.BirdID, [BirdID]/[BirdID] AS wasAnHelper, mb_tb0.age
FROM mb_tb0
WHERE (((mb_tb0.BirdID)>0) AND ((mb_tb0.Status)="H"));
Run and save as table mb_ageAsHelp
SELECT DISTINCT mb_birthsAndAge.BirdID, Min(mb_birthsAndAge.age) AS minimumAgeAsHelp, mb_birthsAndAge.wasAnHelper
FROM mb_birthsAndAge
GROUP BY mb_birthsAndAge.BirdID, mb_birthsAndAge.wasAnHelper;
```

Then run and save qrWasHelper_mb and save as mb_tbHelp
```
SELECT DISTINCT mb_tb0.BirdID, [BirdID]/[BirdID] AS wasAnHelper
FROM mb_tb0
WHERE (((mb_tb0.BirdID)>0) AND ((mb_tb0.Status)="H"));
```

Then run and save qr4_mb and save also as a table (from Design View make a table then run) name mb_tb1:

```
SELECT mb_tb0.BirdID, mb_tb0.FieldPeriodID, mb_tb0.Status, mb_tb0.BreedGroupID, mb_tb0.PeriodEnd, mb_tb0.TerritoryNumber, mb_tb0.SexEstimate, mb_tb0.LastObsBeforeDeatn, mb_tb0.deathYear, mb_tb0.deathAge, mb_tb0.age, mb_tb0.BirthDate, mb_tb0.ReleaseIsland, IIf([wasAnHelper]=1,1,0) AS hasBeenH
FROM mb_tb0 LEFT JOIN mb_tbHelp ON mb_tb0.BirdID = mb_tbHelp.BirdID;
```
Runa and save qrTQ_mb
```
SELECT DISTINCTROW tblBreedGroupLocation.BreedGroupID, tblTerritories.Island, tblTerritories.TerritoryNumber, tblBreedGroupLocation.FieldPeriodID, tblBreedGroupLocation.TerritoryID
FROM tblTerritories INNER JOIN tblBreedGroupLocation ON tblTerritories.TerritoryID = tblBreedGroupLocation.TerritoryID
WHERE (((tblTerritories.Island)="CN"));
```
Then run anda save qr5_mb
```
SELECT mb_tb1.BirdID, mb_tb1.FieldPeriodID, mb_tb1.Status, mb_tb1.BreedGroupID, mb_tb1.PeriodEnd, mb_tb1.TerritoryNumber, mb_tb1.SexEstimate, mb_tb1.LastObsBeforeDeatn, mb_tb1.deathYear, mb_tb1.deathAge, mb_tb1.age, mb_tb1.BirthDate, mb_tb1.hasBeenH, mb_tb1.ReleaseIsland
FROM mb_tb1 LEFT JOIN qrTQ_mb ON mb_tb1.BreedGroupID = qrTQ_mb.BreedGroupID;
```
From the query wizard make a cross tab, to count how many individuals per status in each breed group.  Save as qr6_mb. The sql code is
```
TRANSFORM Count(mb_tb1.[BirdID]) AS CountOfBirdID
SELECT mb_tb1.[BreedGroupID], Count(mb_tb1.[BirdID]) AS [Total Of BirdID]
FROM mb_tb1
GROUP BY mb_tb1.[BreedGroupID]
PIVOT mb_tb1.[Status];
Then run thia and save as qr7_mb and save as mb_tb_qr7
SELECT qr6_mb.BreedGroupID, qr6_mb.[Total Of BirdID], IIf([AB] Is Null,0,[AB]) AS numOfAB, IIf([H] Is Null,0,[H]) AS numOfH, IIf([ABX] Is Null,0,[ABX]) AS numOfABX
FROM qr6_mb;
```
Then extract the data and save as mb_tb2
```
SELECT mb_tb1.BirdID, mb_tb1.FieldPeriodID, mb_tb1.Status, mb_tb1.BreedGroupID, mb_tb1.PeriodEnd, mb_tb1.TerritoryNumber, mb_tb1.SexEstimate, mb_tb1.LastObsBeforeDeatn, mb_tb1.deathYear, mb_tb1.deathAge, mb_tb1.age, mb_tb1.BirthDate, mb_tb1.ReleaseIsland, mb_tb1.hasBeenH, tblTerritories.TerritoryID, mb_ageAsHelp.minimumAgeAsHelp
FROM (mb_tb1 LEFT JOIN tblTerritories ON mb_tb1.TerritoryNumber = tblTerritories.TerritoryNumber) LEFT JOIN mb_ageAsHelp ON mb_tb1.BirdID = mb_ageAsHelp.BirdID
WHERE (((tblTerritories.Island)="CN"));
```

Run and save qrCatches_mb
```
SELECT tblCatches.BirdID, tblCatches.BodyMass, tblCatches.RightTarsus, tblCatches.WingLength, sys_OccasionPlusFieldPeriod.Island, sys_OccasionPlusFieldPeriod.FieldPeriodID, sys_OccasionPlusFieldPeriod.OccasionDate, tblCatches.Ageclass, sys_OccasionPlusFieldPeriod.Observer, tblCatches.HeadBill, tblFieldPeriodIDs.Year
FROM (tblCatches INNER JOIN sys_OccasionPlusFieldPeriod ON tblCatches.OccasionID = sys_OccasionPlusFieldPeriod.OccasionID) INNER JOIN tblFieldPeriodIDs ON sys_OccasionPlusFieldPeriod.FieldPeriodID = tblFieldPeriodIDs.FieldPeriodID
WHERE (((sys_OccasionPlusFieldPeriod.Island)="CN") AND ((sys_OccasionPlusFieldPeriod.OccasionDate)>#1/1/1994#) AND ((tblCatches.Ageclass)<>"CH"));
```
Then add sex and extract as catches
```
SELECT qrCatches_mb.BirdID, qrCatches_mb.BodyMass, qrCatches_mb.RightTarsus, qrCatches_mb.WingLength, qrCatches_mb.Island, qrCatches_mb.FieldPeriodID, qrCatches_mb.OccasionDate, qrCatches_mb.Ageclass, qrCatches_mb.Observer, qrCatches_mb.Year, usys_qSexEstimate.SexEstimate
FROM qrCatches_mb INNER JOIN usys_qSexEstimate ON qrCatches_mb.BirdID = usys_qSexEstimate.BirdID;
Extract territory size and insXveg counts save as terr_quality.csv
SELECT tblBreedGroupLocation.FieldPeriodID, tblBreedGroupLocation.BreedGroupID, tblTerritories.TerritoryNumber, tblTerritories.Island, usys_qTerrQualityUngrouped.TerritorySize, usys_qTerrQualityUngrouped.SumVeg_X_Insects, tblFieldPeriodIDs.PeriodStart
FROM tblFieldPeriodIDs INNER JOIN ((tblTerritories INNER JOIN tblBreedGroupLocation ON tblTerritories.TerritoryID = tblBreedGroupLocation.TerritoryID) INNER JOIN usys_qTerrQualityUngrouped ON tblTerritories.TerritoryID = usys_qTerrQualityUngrouped.TerritoryID) ON (usys_qTerrQualityUngrouped.FieldPeriodID = tblFieldPeriodIDs.FieldPeriodID) AND (tblFieldPeriodIDs.FieldPeriodID = tblBreedGroupLocation.FieldPeriodID)
WHERE (((tblTerritories.Island)="CN") AND ((tblFieldPeriodIDs.PeriodStart)>#1/1/1994#));
```



To get genotypes runa dn save as

mb_gen and extract as mhc.csv remember it’s full of duplicates!!!!
```
SELECT tblGenotype.BirdID, tblGenotype.Locus, tblGenotype.AlleleA, tblGenotype.AlleleB
FROM tblGenotype
ORDER BY tblGenotype.BirdID, tblGenotype.Locus;
```


Get terr size gis export territorySizeArcGIS
```
SELECT tblTerritories.TerritoryID, tblTerritories.TerritoryNumber, tblTerritories.Island, tblTerritorySizes.SizeMethod, tblTerritorySizes.TerritorySize
FROM tblTerritories INNER JOIN tblTerritorySizes ON tblTerritories.TerritoryID = tblTerritorySizes.TerritoryID
WHERE (((tblTerritories.Island)="CN") AND ((tblTerritorySizes.SizeMethod)="D"));
```

Death year:
```
SELECT tblBirdID.BirthDate, Year(tblBirdID.BirthDate) AS BirthYear, tblBreedGroupLocation.FieldPeriodID, tblFieldPeriodIDs.PeriodYear, tblFieldPeriodIDs.PeriodStart, tblBirdID.BirdID, tblBreedStatus.Status, tblFieldPeriodIDs.Island
FROM tblFieldPeriodIDs RIGHT JOIN (tblBirdID LEFT JOIN (tblBreedGroupLocation RIGHT JOIN tblBreedStatus ON tblBreedGroupLocation.BreedGroupID = tblBreedStatus.BreedGroupID) ON tblBirdID.BirdID = tblBreedStatus.BirdID) ON tblFieldPeriodIDs.FieldPeriodID = tblBreedGroupLocation.FieldPeriodID;
```
