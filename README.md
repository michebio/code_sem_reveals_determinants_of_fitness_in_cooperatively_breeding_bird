Description: Scripts and datasets used to reproduce analyses and graphs presented in the manuscript

Related publication: Structural equation modeling reveals determinants of fitness in a cooperatively breeding bird

Subject: Medicine, Health and Life Sciences

Authors:
Michela Busana [a], [https://orcid.org/0000-0002-3806-8575](https://orcid.org/0000-0002-3806-8575)

Franz J Weissing [a,b], [https://orcid.org/0000-0003-3281-663X](https://orcid.org/0000-0003-3281-663X)

Martijn Hammers [a,c], [https://orcid.org/0000-0002-6638-820X](https://orcid.org/0000-0002-6638-820X)

Joke Bakker [a], [https://orcid.org/0000-0002-0439-8752](https://orcid.org/0000-0002-0439-8752) 

Hannah L Dugdale [a,d],  [https://orcid.org/0000-0001-8769-0099](https://orcid.org/0000-0001-8769-0099) 

Sara Raj Pant [a,e], [https://orcid.org/0000-0003-4168-7390](https://orcid.org/0000-0003-4168-7390)

David S Richardson [e,f], [https://orcid.org/0000-0001-7226-9074](https://orcid.org/0000-0001-7226-9074)

Terrence A Burke [g], [https://orcid.org/0000-0003-3848-1244](https://orcid.org/0000-0003-3848-1244) 

Jan Komdeur [a], [https://orcid.org/0000-0002-9241-0124](https://orcid.org/0000-0002-9241-0124)

Affiliations:
[a] Groningen Institute for Evolutionary Life Sciences, University of Groningen, Groningen, The Netherlands 

[b] Netherlands Institute for Advanced Study, Amsterdam, The Netherlands

[c] Aeres University of Applied Sciences, Almere, The Netherlands 

[d] The Faculty of Biological Sciences, University of Leeds, Leeds, United Kingdom 

[e] School of Biological Sciences, University of East-Anglia, United Kingdom 

[f] Nature Seychelles, PO Box 1310, Roche Caiman, Mah\`e, Republic of Seychelles 

[g] Department of Animal and Plant Sciences, University of Sheffield, Sheffield, United Kingdom 

Code is written and maintained by Michela Busana.

#  Metadata

All analyses were run in [R (v. 4.4.0)](https://cran.r-project.org/) and and JAGS-4.3.0.

Code is organized in multiple sub-folders. 
Files typically start with a brief explanation of their use.
Please, start the project by opening the `.Rproj` file in [RStudio](https://support.rstudio.com/hc/en-us/articles/200526207-Using-Projects) to automatically set reproducible relative paths to files.


Folder structure:

*   models contains scripts to calculate parameter estimates and the L_vu measure for the ten models. Within the folder there are twelve subfolders:
	- `M1` to `M10` contain scripts to run models M1 to M10. Each folder includes multiple scripts: 
		*   `m1.jag` contains JAGS code
		*   `m1.R` is the R script to run the JAGS code
		*   `sa.R` is the R script to save all relevant outputs to calculate the L_vu measure
		*   `lv_test.R` is the R script to extract the L_vu(0.5) and L_vu(0.8) measures
	-   `src_lv_scripts` contains scripts to calculate the L_vu measure
	-   `thresholds` contains scripts to calculate the thresholds for the variables number of helpers, group size, insect availability and territory size
*   `comparison_with_previous_studies` contains scripts to run formal comparisons with previous studies using GLM/GLMM:
  - These scripts use the data file `"cleanData/original_data/data_extraGroup_with_fledge.txt"` as a source file for data
*   forestplot contains scripts to generate the forest plot included in the manuscript
*   cleanData contains information on extracting the data from the Seychelles warbler database (version 0.61 beta2). This folder is meant for use only to people that have access to the Seychelles warbler database:
      -    the Seychelles warbler database is stored at `http://warblerdata.webhosting.rug.nl/tiki-index.php`
      -    the SQL code used to extract the data from the main database is written on `SQL_code.md`
*   the dataset to be read in JAGS is saved as `data.Rdata`      



#   Reading the dataset


Data on Seychelles warblers are saved in `data.Rdata` file and `cleanData/original_data/data_extraGroup_with_fledge.txt`.

```r
load('data.Rdata') #   the data.Rdata is a list of elements:
#    "data" (see below) are the data ordered and organised to be read by the JAGS code,
#    "ch_size" (chain size),
#    "burnIn" (burn in length),
#    "thin_interval" (thin interval)

data <- info[['data']]
```

##   Necessary metadata to interpret the data

The data (`data.Rdata` file) is a list with elements: z1, z2, fixf, fixm, kkF, kkM, Nf, Nm, numFF, numMM, z1c, z2c, dimc, thd3, thd4, thd5

*    z1 and z2 contain the data points for dominant females and males, respectively. Data are ordered by individual unique ID and time. Within z1 and z2 there are seven columns:
     *    Column 1 = number of offspring born intra-group;
     *    Column 2 = number of offspring born extra-group;
     *    Column 3 = number of offspring with only one known parent;
     *    Column 4 = group size;
     *    Column 5 = number of helpers;
     *    Column 6 = territory size;
     *    Column 7 = insect availability.

*   fixf and fixm contain the fixed effects in the model (age and age^2) of females and males, respectively

*   kkF and kkM are identifiers equal to the row of the data belonging to a specific individual, female or male, respectively, minus one. This value is used in JAGS to extract the within-individual variation.

*   Nf and Nm hold the number of observation for each individual. The minimum number is two because all individuals with only one observation were excluded a priori from the dataset

*    numFF and numMM are the total number of dominant females and males, respectively

*    z1c z2c contain the social pair id of females and males and to be associated with the z1 and z2 data

*    dimc is the total number of social pairs in the data

*    thd3  thd4  thd5 hold the threshold values for the ordered categorical variables. the number 3, 4, and 5 indicate the number of categories per variable:
     *    thd3 is a matrix of 5 rows and 4 columns holding the thresholds for Column 1 to Column 5 in z1 and z2
     *    thd 4 is a matrix of 1 row and 5 columns holding threshold values for territory size (Column 6 in z1 and z2)
     *    thd5 is a matrix of 1 row and 6 columns holding threshold values for insect availability (Column 7 in z1 and z2)
     *    Note that because JAGS does not recognise the value 0 within the `dinterval()` function, each value of ordered categorical variables was augmented by 1.
     
##   Additional dataset

To run the GLM/GLMM use the data file `cleanData/original_data/data_extraGroup_with_fledge.txt`

The file contains the following columns:

*   "SexEstimate" = 0 females, 1 males 
*   "BirdID" = unique identifier for each bird
*   "age_count"  = individual's age
*   "TerritorySize" = size of territory in m^2
*   "TerritoryNumber" = unique identifier for each territory
*   "SumVeg_X_Insects" = insect availability 
*   "year"  = year
*   "Status" = status of the bird. either "BrF"/breeding female or "BrM"/breeding male
*   "groupSize" = size of the group
*   "original_num_h" = number of helpers in the group
*   "nOff_perYEAR" = number of offspring (offspring of at least 12 months of age) produced per year
*   "nFledge__perYEAR" = number of fledglings (offspring of at least 3 months of age) produced per year



# License

Copyright 2021 Michela Busana and University of Groningen

The code in this repository is licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

