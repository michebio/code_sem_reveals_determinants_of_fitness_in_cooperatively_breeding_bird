library(viridis)
library(extrafontdb)
library(extrafont)
library(Rttf2pt1)
loadfonts(device="postscript")
vals <- c(0.2, 0.3, 0.5)
#tiff("forestplot/plot_ordered_var.tiff", res=300, family="Arial", width = 15, height = 10, units = 'cm')
postscript(file="forestplot/plot_ordered_var.eps", family="Arial", horiz=F,onefile=F)

cx= 0.9
par(mar=c(5,4,1,2), mfrow=c(2,1))


# Basic Barplot
my_bar <- barplot(vals, names.arg = c("category 1", "category 2", "category 3"), 
                  border=F,
                  las=1, 
                  col=viridis(3) , 
                  ylim=c(0,0.6) , 
                  main="", cex.names = 1.3,ylab= "", axes=F )
mtext("Proportion", side = 2, adj=0.2)
# Add abline
abline(v=c(4.9 , 9.7) , col="grey")

# Add the text 
text(my_bar, vals +0.03, paste0(vals) ,cex= cx) 

#Legende
legend("topright", 
       legend = "(a)",
        bty = "n", cex = cx, horiz = FALSE)
#c("Proportions of each category of a three-leve ordinal variable z")

x <- rnorm(10000000, 0 , 1)
sx <- summary(x)

dens_x <- density(x)
# Kernel Density of variable
plot(dens_x, main="", xlab = "", axes = F, ylab = "")
mtext("Density", side = 2, adj=0.2)
x_0 <- round(min(which(dens_x$x >= min(x))), 2)                     # Define lower limit of colored area
x_1 <- round(max(which(dens_x$x < sx[2])), 2)                         # Define upper limit of colored area
x_2 <- round(max(which(dens_x$x <= mean(x))), 2)
x_3 <- round(max(which(dens_x$x < max(x))), 2)

with(dens_x,                                               # Add color between two limits
       polygon(x = c(x[c(x_0, x_0:x_1, x_1)]),
               y = c(0, y[x_0:x_1], 0),
               col = viridis(3)[1]))
     

with(dens_x,  polygon(x = c(x[c(x_1, x_1:x_2, x_2)]),
             y = c(0, y[x_1:x_2], 0),
             col = viridis(3)[2]))
     

with(dens_x,  polygon(x = c(x[c(x_2, x_2:x_3, x_3)]),
             y = c(0, y[x_2:x_3], 0),
             col = viridis(3)[3]))

text(x=c(sx[2]-0.5, sx[3]-0.35, sx[5]),
     y=rep(0.05, 3), vals, cex=cx)
legend("topright", 
       legend = "(b)",
       bty = "n", cex = cx, horiz = FALSE)
legend(x=1.5, y=0.4, as.expression(bquote(italic("N[0,"~ sigma ^2 ~"]"))), lwd=1, bty="n")# bquote(italic("N"), "[0, 1]"), lwd=2, bty="n")
poss <- round(dens_x$x[c(x_0, x_1, x_2, x_3)],1)

axis(1, at = poss, 
     labels = c(expression(paste(alpha[0],  " = -", infinity)),
                bquote(alpha[1] == "-.7" ~ "       "),
                bquote("    " ~ alpha[2] == ".0" ),
                expression(paste(alpha[3],  " = ", infinity))), cex=cx)

dev.off()

