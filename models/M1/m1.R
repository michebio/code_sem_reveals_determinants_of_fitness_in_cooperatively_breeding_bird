rm(list=ls())
#setwd('/home/michela/DATA/demogModel/MALES/LATEST/trial')

numNodes <- 5
set.seed(2007)
library(doMC);
library(rjags); load.module('lecuyer')
library(random)
library(coda)
# read data in
load('data.Rdata') #the data.Rdata is a list of elements:
# "data" (see below) are the data ordered and organised to be read by the JAGS code,
# "ch_size" (chain size),
# "burnIn" (burn in length),
# "thin_interval" (thin interval)

data <- info[['data']]
source("models/runtime.R")
setwd("models/M1/")
#*************************************************************************************#
###             Necessary metadata to interpret the data                            ###
#*************************************************************************************#
# The data is a list itself with elements: z1, z2, fixf, fixm, kkF, kkM, Nf, Nm, numFF,
# numMM, z1c, z2c, dimc, thd3, thd4, thd5

# z1 and z2 contain the data points for dominant females and males respectively. Data are ordered by individual unique ID and
# time. Within z1 and z2 there are seven columns:
     # Col 1 = number of offspring born intra-group;
     # Col 2 = number of offspring born extra-group;
     # Col 3 = number of offspring with only one known parent;
     # Col 4 = group size;
     # Col 5 = number of helpers;
     # Col 6 = territory size;
     # Col 7 = insect availability.

# fixf and fixm contain the fixed effects in the model (age and age^2) of females and males respectively

# kkF and kkM are identifiers equal to the the row of the data belonging to a specific individual female or male respecitively
# minus one. This value is used in JAGS to extract the within-individual variation.

# Nf and Nm hold the number of observation for each individual. The minimum number is two because all individuals with only
# one observation where excluded a priori from the dataset

# numFF and numMM are the total number of dominant females and males respectively

# z1c z2c contain the social pair id of females and males and to be associated with the z1 and z2 data

# dimc is the total number of social pairs in the data

# "thd3"  "thd4"  "thd5" hold the threshold values for the ordered categorical variables. the number 3, 4, and 5 indicate the
# number of categories per variable:
     # thd3 is a matrix of 5 rows and 4 columns holding the thresholds for col 1 to col 5 in z1 and z2
     # thd 4 is a matrix of 1 row and 5 columns holding threshold values for territory size (col 6 in z1 and z2)
     # thd5 is a matrix of 1 row and 6 columns holding threshold values for insect availability (col 7 in z1 and z2)
# function that specifies initial values for the chains
inits <- list()
inits[[1]]<-function(){
list(
psi_1F=rep(.5,7),
gam1F=rep(.5,2),  gam2F=rep(.5,2),
muF=rep(.5,7),
phi_2f=(rep(.5,2)),phi_1f=(rep(.5,2)),ddthatf1inv=.5,ddthatf2inv=.5,
lbF=c(NA, rep(.5,2),NA,.5,NA,.5),lbC=c(NA, rep(.5,2),NA,.5,NA,.5),lbM=c(NA, rep(.5,2),NA,.5,NA,.5),
lwF=c(NA, rep(.5,2),NA,.5,NA,.5),lwM=c(NA, rep(.5,2),NA,.5,NA,.5),

psi_1M=rep(.5,7),
gam1M=rep(.5,2),   gam2M=rep(.5,2),
muM=rep(.5,7),muC=rep(.5,7),
phi_2m=(rep(.5,2)),phi_1m=(rep(.5,2)),ddthatm1inv=.5,ddthatm2inv=.5,
 .RNG.name="lecuyer::RngStream",
 .RNG.seed=5)}

 inits[[2]]<-function(){
list(
psi_1F=rep(30,7),
gam1F=rep(30,2),  gam2F=rep(30,2),
muF=rep(30,7),
phi_2f=(rep(30,2)),phi_1f=(rep(30,2)),ddthatf1inv=30,ddthatf2inv=30,
lbC=c(NA, rep(30,2),NA,30,NA,30),
lwF=c(NA, rep(30,2),NA,30,NA,30),lbF=c(NA, rep(30,2),NA,30,NA,30),

psi_1M=rep(30,7), lwM=c(NA, rep(30,2),NA,30,NA,30),lbM=c(NA, rep(30,2),NA,30,NA,30),
gam1M=rep(30,2),  gam2M=rep(30,2),
muM=rep(30,7),muC=rep(30,7),
phi_2m=(rep(30,2)),phi_1m=(rep(30,2)),ddthatm1inv=30,ddthatm2inv=30,
 .RNG.name="lecuyer::RngStream",
 .RNG.seed=2)}



inits[[3]]<- function(){
list(
psi_1F=rep(50,7),
gam1F=rep(-20,2),  gam2F=rep(-20,2),
muF=rep(-20,7),
phi_2f=(rep(50,2)),phi_1f=(rep(50,2)),ddthatf1inv=50,ddthatf2inv=50,
lbF=c(NA, rep(-20,2),NA,-20,NA,-20),
lwF=c(NA, rep(-20,2),NA,-20,NA,-20),
psi_1M=rep(50,7),
gam1M=rep(-20,2),gam2M=rep(-20,2),
muM=rep(-20,7),muC=rep(-20,7),
phi_2m=(rep(50,2)),phi_1m=(rep(50,2)),ddthatm1inv=50,ddthatm2inv=50,lbC=c(NA, rep(-20,2),NA,-20,NA,-20),
lbM=c(NA, rep(-20,2),NA,-20,NA,-20),
lwM=c(NA, rep(-20,2),NA,-20,NA,-20),
 .RNG.name="lecuyer::RngStream",
 .RNG.seed=8)}

 inits[[4]]<- function(){
list(
psi_1F=rep(5,7),
gam1F=rep(-2,2),  gam2F=rep(-2,2),
muF=rep(-2,7),
phi_2f=(rep(5,2)),phi_1f=(rep(5,2)),ddthatf1inv=5,ddthatf2inv=5,
lbF=c(NA, rep(-2,2),NA,-2,NA,-2),
lwF=c(NA, rep(-2,2),NA,-2,NA,-2),
psi_1M=rep(5,7),
gam1M=rep(-2,2),  gam2M=rep(-2,2),
muM=rep(-2,7),muC=rep(-2,7),
phi_2m=(rep(5,2)),phi_1m=(rep(5,2)),ddthatm1inv=5,ddthatm2inv=5,lbC=c(NA, rep(-2,2),NA,-2,NA,-2),
lbM=c(NA, rep(-2,2),NA,-2,NA,-2),
lwM=c(NA, rep(-2,2),NA,-2,NA,-2),
 .RNG.name="lecuyer::RngStream",
 .RNG.seed=10)}


 inits[[5]]<-function(){
list(
psi_1F=rep(10,7),
gam1F=rep(10,2),  gam2F=rep(10,2),
muF=rep(10,7),
phi_2f=(rep(10,2)),phi_1f=(rep(10,2)),ddthatf1inv=10,ddthatf2inv=10,

psi_1M=rep(10,7),
gam1M=rep(10,2),  gam2M=rep(10,2),
muM=rep(10,7),muC=rep(10,7),
phi_2m=(rep(10,2)),phi_1m=(rep(10,2)),ddthatm1inv=10,ddthatm2inv=10, lbC=c(NA, rep(10,2),NA,10,NA,10),
lbM=c(NA, rep(10,2),NA,10,NA,10),
lwM=c(NA, rep(10,2),NA,10,NA,10),
lbF=c(NA, rep(10,2),NA,10,NA,10),
lwF=c(NA, rep(10,2),NA,10,NA,10),
 .RNG.name="lecuyer::RngStream",
 .RNG.seed=4)}
# Function to combine multiple mcmc lists into a single one
mcmc.combine <- function( ... ){
  return( as.mcmc.list( sapply( list( ... ),mcmc ) ) )
}


registerDoMC(numNodes)
m <- foreach(i=1:numNodes, .inorder=FALSE,
                           .packages=c('rjags','random'),
                           .combine='mcmc.combine',
                           .multicombine=TRUE) %dopar% {
load.module("lecuyer")
model_JAGS <- jags.model("m1.jag", data=data, inits[[i]], n.chains = 1, n.adapt=adapt)#inits[[i]]
update(model_JAGS, burnIn)
m_full <- coda.samples(model_JAGS,c("psi_c", "phi_c", "lbC","muC",
"gam1F","gam1M","gam2F","gam2M","muM","muF","lbF","lwF","lbM","lwM","psi_1F","phi_1f","phi_2f","psi_1idtf","psi_2dtf","psi_1M","phi_1m","phi_2m","psi_1idtm","psi_2dtm","b"),n.iter=ch_size,thin=thin_interval)
return(m_full)
}






save(m, file="jags_coda_samples_DATA.Rdata")

print('gelman-rubin diag 1')
gelman.diag(m,multivariate=F)


s<- summary(m)
save(s,file='summary.RData')
quant <- round(summary(m)$quantiles)
save(quant, file='summaryQuantiles.Rdata')# instead of printing them to sceen

hpd<-HPDinterval( as.mcmc(do.call(rbind,m)) )
save(hpd,file='hpd_Interval.RData')
print('hpd')
hpdp<-cbind(round(hpd[,'lower'],3),round(hpd[,'upper'],3))
print(hpdp)

pdf('plot_samples_excludingBurnIn.pdf')
plot(m)
dev.off()



print('Mean and SD')
print(cbind(round(s$statistics[,'Mean'],3),round(s$statistics[,'SD'],3)))




info <- list()
info[['data']] <- data
info[['ch_size']] <- ch_size
info[['burnIn']] <- burnIn
info[['thin_interval']] <- thin_interval
save(info, file='data.Rdata')


print('summary')
print(round(s$statistics,3))
