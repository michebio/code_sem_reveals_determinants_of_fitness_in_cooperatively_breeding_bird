rm(list = ls())

#proj_dir <- "/home/michela/Data/git/thesis/scripts/code_sem_reveals_determinants_of_fitness_in_cooperatively_breeding_bird"
#setwd(proj_dir)

library(doMC)
library(rjags)
load.module("lecuyer")
library(random)
library(coda)
test <- TRUE
test <- FALSE
print(paste("testing == ", test))
# read data in
load("data.Rdata")  #the data.Rdata is a list of elements:
# 'data' (see below) are the data ordered and organised to be read by the JAGS code, 'ch_size' (chain size), 'burnIn' (burn in length),
# 'thin_interval' (thin interval)
data <- info[["data"]]
source("models/runtime.R")
#source("models/testing.R")
setwd("models/M8/")


# function that specifies initial values for the chains
inits <- list()
inits <- list()
inits[[1]] <- function() {
   list(psi_1F = rep(0.5, 7), pi1 = 0.5, pi2 = 0.5,
       
        gam1F = rep(0.5, 1), gam2F = rep(0.5, 1), mu = rep(0.5, 7),
        gam1M = rep(0.5, 1), gam2M = rep(0.5, 1),  
        phi_2f = (rep(0.5, 1)), phi_1f = (rep(0.5, 1)), 
        ddthatf1inv = rep(0.5, 2), ddthatf2inv = rep(0.5, 2), 
        lb = c(NA, rep(0.5, 2), NA, 0.5, NA, 0.5), lbC = c(NA, rep(0.5, 2), NA, 0.5, NA, 0.5), 
        lw = c(NA, rep(0.5, 2), NA, 0.5, NA, 0.5), 
        psi_1M = rep(0.5, 7), phi_2m = (rep(0.5, 1)), 
        phi_1m = (rep(0.5, 1)), 
        ddthatm1inv = rep(0.5, 2), ddthatm2inv = rep(0.5, 2), 
        .RNG.name = "lecuyer::RngStream", .RNG.seed = 5)
}

inits[[2]] <- function() {
   list(psi_1F = rep(30, 7), pi1 = 30, pi2 = 30, 
     
        gam1F = rep(30, 1), gam2F = rep(30, 1), mu = rep(30, 7), 
        gam1M = rep(30, 1), gam2M = rep(30, 1),
        phi_2f = (rep(30, 1)), phi_1f = (rep(30, 1)), 
        ddthatf1inv = rep(30, 2), ddthatf2inv = rep(30, 2), 
        lb = c(NA, rep(30, 2), NA, 30, NA, 30), lbC = c(NA, rep(30, 2), NA, 30, NA, 30), 
        lw = c(NA, rep(30, 2), NA, 30, NA, 30), 
        psi_1M = rep(30, 7), phi_2m = (rep(30, 1)), 
        phi_1m = (rep(30, 1)), 
        ddthatm1inv = rep(30, 2), ddthatm2inv = rep(30, 2),
        .RNG.name = "lecuyer::RngStream", .RNG.seed = 2)
}



inits[[3]] <- function() {
   list(psi_1F = rep(50, 7), pi1 = 50, pi2 = 50,
        
        gam1F = rep(50, 1), gam2F = rep(50, 1), 
        gam1M = rep(50, 1), gam2M = rep(50, 1),
        mu = rep(50, 7), 
        phi_2f = (rep(50, 1)), phi_1f = (rep(50, 1)), 
        ddthatf1inv = rep(50, 2), ddthatf2inv = rep(50, 2), 
        lb = c(NA, rep(50, 2), NA, 50, NA, 50), lbC = c(NA, rep(50, 2), NA, 50, NA, 50), 
        lw = c(NA, rep(50, 2), NA, 50, NA, 50), 
        psi_1M = rep(50, 7), phi_2m = (rep(50, 1)), 
        phi_1m = (rep(50, 1)), 
        ddthatm1inv = rep(50, 2), ddthatm2inv = rep(50, 2),
      .RNG.name = "lecuyer::RngStream", .RNG.seed = 8)
}

inits[[4]] <- function() {
   list(psi_1F = rep(5, 7), pi1 = 5, pi2 = 5, 
   
        gam1F = rep(5, 1), gam2F = rep(5, 1), mu = rep(5, 7), 
         gam1M = rep(5, 1), gam2M = rep(5, 1), 
        phi_2f = (rep(5, 1)), phi_1f = (rep(5, 1)), 
        ddthatf1inv = rep(5, 2), ddthatf2inv = rep(5, 2), 
        lb = c(NA, rep(5, 2), NA, 5, NA, 5), lbC = c(NA, rep(5, 2), NA, 5, NA, 5), 
        lw = c(NA, rep(5, 2), NA, 5, NA, 5), 
        psi_1M = rep(5, 7), phi_2m = (rep(5, 1)), 
        phi_1m = (rep(5, 1)), 
        ddthatm1inv = rep(5, 2), ddthatm2inv = rep(5, 2),
      .RNG.name = "lecuyer::RngStream", .RNG.seed = 10)
}


inits[[5]] <- function() {
   list(psi_1F = rep(10, 7), pi1 = 10, pi2 = 10,  
    
        gam1F = rep(10, 1), gam2F = rep(10, 1), mu = rep(10, 7), 
        gam1M = rep(10, 1), gam2M = rep(10, 1),
        phi_2f = (rep(10, 1)), phi_1f = (rep(10, 1)), 
        ddthatf1inv = rep(10, 2), ddthatf2inv = rep(10, 2), 
        lb = c(NA, rep(10, 2), NA, 10, NA, 10), lbC = c(NA, rep(10, 2), NA, 10, NA, 10), 
        lw = c(NA, rep(10, 2), NA, 10, NA, 10), 
        psi_1M = rep(10, 7), phi_2m = (rep(10, 1)), 
        phi_1m = (rep(10, 1)), 
        ddthatm1inv = rep(10, 2), ddthatm2inv = rep(10, 2),
      .RNG.name = "lecuyer::RngStream", .RNG.seed = 4)
}
# Function to combine multiple mcmc lists into a single one
mcmc.combine <- function(...) {
  return(as.mcmc.list(sapply(list(...), mcmc)))
}



model_JAGS <- jags.model("m1.jag", data = data, inits[[1]], n.chains = 1, n.adapt = adapt)  #inits[[i]]
update(model_JAGS, burnIn)
m <- coda.samples(model_JAGS, c("pi1", "pi2", 
                                "psi1invc", "lbC","xic", "psi_idc","muC","ephatc",
                                "fixm","fixf",
                                "gam1F","gam2F",
                                "gam1M","gam2M",
                                "lb","lw",
                                "psi1invF","psi2invF",
                                "psi_1idtf","psi_2dtf",
                                "psi1invM","psi2invM","psi_1idtm",
                                "psi_2dtm","b","dthatf1","dthatf2",
                                "dthatm1","dthatm2","ephat2f","ephat1F",
                                "ephat2m","ephat1M","xim1","xim2","xif1","xif2",
                                "mu"), n.iter = ch_size, thin = thin_interval)


print(class(m))


m <- m[[1]]
print("dimension m")
print(dim(m))

print(paste("num col= ", length(varnames(m))))

ephatc <- m[, which(substr(varnames(m), 1, 6) == "ephatc")]
save(ephatc, file = "m1_ephatc.RData")
dthatf1 <- m[, which(substr(varnames(m), 1, 7) == "dthatf1")]
save(dthatf1, file = "m1_dthatf1.RData")
dthatf2 <- m[, which(substr(varnames(m), 1, 7) == "dthatf2")]
save(dthatf2, file = "m1_dthatf2.RData")
dthatm1 <- m[, which(substr(varnames(m), 1, 7) == "dthatm1")]
save(dthatm1, file = "m1_dthatm1.RData")
dthatm2 <- m[, which(substr(varnames(m), 1, 7) == "dthatm2")]
save(dthatm2, file = "m1_dthatm2.RData")
ephat2f <- m[, which(substr(varnames(m), 1, 7) == "ephat2f")]
save(ephat2f, file = "m1_ephat2f.RData")
ephat1F <- m[, which(substr(varnames(m), 1, 7) == "ephat1F")]
save(ephat1F, file = "m1_ephat1F.RData")
ephat2m <- m[, which(substr(varnames(m), 1, 7) == "ephat2m")]
save(ephat2m, file = "m1_ephat2m.RData")
ephat1M <- m[, which(substr(varnames(m), 1, 7) == "ephat1M")]
save(ephat1M, file = "m1_ephat1M.RData")
lbM <- lbF <- m[, which(substr(varnames(m), 1, 2) == "lb")]
save(lbM, file = "m1_lbM.RData")
save(lbF, file = "m1_lbF.RData")

pi1M <- pi1F <- m[, which(substr(varnames(m), 1, 3) == "pi1")]
save(pi1M, file = "m1_pi1M.RData")
save(pi1F, file = "m1_pi1F.RData")

pi2M <- pi2F <- m[, which(substr(varnames(m), 1, 3) == "pi2")]
save(pi2M, file = "m1_pi2M.RData")
save(pi2F, file = "m1_pi2F.RData")

lwF <- lwM <- m[, which(substr(varnames(m), 1, 2) == "lw")]
save(lwF, file = "m1_lwF.RData")
save(lwM, file = "m1_lwM.RData")
psi_1idtf <- m[, which(substr(varnames(m), 1, 9) == "psi_1idtf")]
save(psi_1idtf, file = "m1_psi_1idtf.RData")
psi_2dtf <- m[, which(substr(varnames(m), 1, 8) == "psi_2dtf")]
save(psi_2dtf, file = "m1_psi_2dtf.RData")
psi_1idtm <- m[, which(substr(varnames(m), 1, 9) == "psi_1idtm")]
save(psi_1idtm, file = "m1_psi_1idtm.RData")
psi_2dtm <- m[, which(substr(varnames(m), 1, 8) == "psi_2dtm")]
save(psi_2dtm, file = "m1_psi_2dtm.RData")
psi2invM <- m[, which(substr(varnames(m), 1, 8) == "psi2invM")]
save(psi2invM, file = "m1_psi2invM.RData")
psi1invM <- m[, which(substr(varnames(m), 1, 8) == "psi1invM")]
save(psi1invM, file = "m1_psi1invM.RData")
psi2invF <- m[, which(substr(varnames(m), 1, 8) == "psi2invF")]
save(psi2invF, file = "m1_psi2invF.RData")
psi1invF <- m[, which(substr(varnames(m), 1, 8) == "psi1invF")]
save(psi1invF, file = "m1_psi1invF.RData")
xif1 <- m[, which(substr(varnames(m), 1, 4) == "xif1")]
save(xif1, file = "m1_xif1.RData")
xif2 <- m[, which(substr(varnames(m), 1, 4) == "xif2")]
save(xif2, file = "m1_xif2.RData")
xim1 <- m[, which(substr(varnames(m), 1, 4) == "xim1")]
save(xim1, file = "m1_xim1.RData")
xim2 <- m[, which(substr(varnames(m), 1, 4) == "xim2")]
save(xim2, file = "m1_xim2.RData")
gam1F <- m[, which(substr(varnames(m), 1, 5) == "gam1F")]
save(gam1F, file = "m1_gam1F.RData")
gam1M <- m[, which(substr(varnames(m), 1, 5) == "gam1M")]
save(gam1M, file = "m1_gam1M.RData")
gam2F <- m[, which(substr(varnames(m), 1, 5) == "gam2F")]
save(gam2F, file = "m1_gam2F.RData")
gam2M <- m[, which(substr(varnames(m), 1, 5) == "gam2M")]
save(gam2M, file = "m1_gam2M.RData")
muF <- muM <- m[, which(substr(varnames(m), 1, 2) == "mu")]
save(muF, file = "m1_muF.RData")
save(muM, file = "m1_muM.RData")

b <- m[, which(substr(varnames(m), 1, 1) == "b")]
save(b, file = "m1_b.RData")
fixm <- m[, which(substr(varnames(m), 1, 4) == "fixm")]
save(fixm, file = "m1_fixm.RData")
fixf <- m[, which(substr(varnames(m), 1, 4) == "fixf")]
save(fixf, file = "m1_fixf.RData")
psi1invc <- m[, which(substr(varnames(m), 1, 8) == "psi1invc")]
save(psi1invc, file = "m1_psi1invc.RData")
lbC <- m[, which(substr(varnames(m), 1, 3) == "lbC")]
save(lbC, file = "m1_lbC.RData")
xic <- m[, which(substr(varnames(m), 1, 3) == "xic")]
save(xic, file = "m1_xic.RData")
psi_idc <- m[, which(substr(varnames(m), 1, 7) == "psi_idc")]
save(psi_idc, file = "m1_psi_idc.RData")
muC <- m[, which(substr(varnames(m), 1, 3) == "muC")]
save(muC, file = "m1_muC.RData")

info <- list()
info[["data"]] <- data
info[["ch_size"]] <- ch_size
info[["burnIn"]] <- burnIn
info[["thin_interval"]] <- thin_interval
save(info, file = "data.Rdata")

source("lv.R")
print("lv calculated")
junk <- dir(path="./",  pattern="m1_", full.names = T) # ?dir
file.remove(junk) # ?file.remove
