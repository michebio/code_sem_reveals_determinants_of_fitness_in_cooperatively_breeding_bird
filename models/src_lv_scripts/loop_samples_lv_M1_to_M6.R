Gammas_F1i <- Gammas_F2 <- matrix(0, nrow = P1, ncol = P2)
Gammas_M1i <- Gammas_M2 <- matrix(0, nrow = P1, ncol = P2)
Lambda_F1i <- Lambda_F2 <- matrix(0, nrow = P, ncol = c(P2 + P1))
Lambda_M1i <- Lambda_M2 <- Lambda_C <- matrix(0, nrow = P, ncol = c(P2 + 
                                                                      P1))

# all threse to be created before the start of loop with m


# calculations
cat3_zFF <- res_3catFF <- array(0, dim = c(nObsFF, c(Ps3 * 3)))
cat4_zFF <- res_4catFF <- array(0, dim = c(nObsFF, c(Ps4 * 4)))
cat5_zFF <- res_5catFF <- array(0, dim = c(nObsFF, (Ps5 * 5)))
CovF <- matrix(0, nrow = P, ncol = P)
store_yF_io <- mu_gi_hat_F <- array(0, dim = c(nObsFF, P))
mu_giFF <- array(0, dim = c(nObsFF, P, width))
TR_FF <- rep(0, nObsFF)
mmuFF <- matrix(0, nrow = nObsFF, ncol = P)
cat3_zMM <- res_3catMM <- array(0, dim = c(nObsMM, c(Ps3 * 3)))
cat4_zMM <- res_4catMM <- array(0, dim = c(nObsMM, c(Ps4 * 4)))
cat5_zMM <- res_5catMM <- array(0, dim = c(nObsMM, (Ps5 * 5)))
CovM <- matrix(0, nrow = P, ncol = P)
store_yM_io <- mu_gi_hat_M <- array(0, dim = c(nObsMM, P))
# yM_io <- array(0, dim=c(nObsMM,P,width)) yF_io <- array(0,
# dim=c(nObsFF, P, width))
mu_giMM <- array(0, dim = c(nObsMM, P, width))
TR_MM <- rep(0, nObsMM)
mmuMM <- matrix(0, nrow = nObsMM, ncol = P)
sumPsi_M <- matrix(0, ncol = P, nrow = width)
sumPsi_F <- matrix(0, ncol = P, nrow = width)
# mu_starM <- array(0, dim=c(nObsMM,c(30),width)) mu_star_3catM <-
# array(0, dim=c(nObsMM,c(Ps3*3),width)) mu_star_2catM <-
# array(0,dim=c(nObsMM,(Pd*2),width)) mu_star_4catM <- array(0,
# dim=c(nObsMM,c(Ps4*4),width)) mu_star_5catM <-
# array(0,dim=c(nObsMM,(Ps5*5), width)) mu_star_3catF <- array(0,
# dim=c(nObsFF,c(Ps3*3),width)) mu_star_2catF <-
# array(0,dim=c(nObsFF,(Pd*2),width)) mu_star_4catF <- array(0,
# dim=c(nObsFF,c(Ps4*4),width)) mu_star_5catF <-
# array(0,dim=c(nObsFF,(Ps5*5), width)) mu_starF <-
# array(0,dim=c(nObsFF,30, width))

for(m in 1:width)
{
  #print(paste("m = ", m))
  
  Gammas_F1i[1,1] <- Gam1F[1,m]
  Gammas_F1i[1,2] <- Gam1F[2,m]
  
  Gammas_F2[1,1] <- Gam2F[1,m]
  Gammas_F2[1,2] <- Gam2F[2,m]
  
  Gammas_M1i[1,1] <- Gam1M[1,m]
  Gammas_M1i[1,2] <- Gam1M[2,m]
  
  Gammas_M2[1,1] <- Gam2M[1,m]
  Gammas_M2[1,2] <- Gam2M[2,m]
  
  Lambda_C[1,1] <- Lam_c[1,m]; #eta1
  Lambda_C[2,1] <- Lam_c[2,m];
  Lambda_C[3,1] <- Lam_c[3,m];
  Lambda_C[4,2] <- Lam_c[4,m] ;
  Lambda_C[5,2] <- Lam_c[5,m] ;
  Lambda_C[6,3] <- Lam_c[6,m] ;
  Lambda_C[7,3] <- Lam_c[7,m] ;
  
  Lambda_F1i[1,1] <- Lam_1iF[1,m]; #eta1
  Lambda_F1i[2,1] <- Lam_1iF[2,m];
  Lambda_F1i[3,1] <- Lam_1iF[3,m];
  Lambda_F1i[4,2] <- Lam_1iF[4,m] ; #xi2
  Lambda_F1i[5,3] <- Lam_1iF[5,m] ;
  Lambda_F1i[6,3] <- Lam_1iF[6,m] ; 	#xi1
  Lambda_F1i[7,3] <- Lam_1iF[7,m] ;
  
  Lambda_F2[1,1] <- Lam_2F[1,m]; #eta1
  Lambda_F2[2,1] <- Lam_2F[2,m];
  Lambda_F2[3,1] <- Lam_2F[3,m];
  Lambda_F2[4,2] <- Lam_2F[4,m] ;
  Lambda_F2[5,2] <- Lam_2F[5,m] ;
  Lambda_F2[6,3] <- Lam_2F[6,m] ;
  Lambda_F2[7,3] <- Lam_2F[7,m] ;
  
  Lambda_M1i[1,1] <- Lam_1iM[1,m]; #eta1
  Lambda_M1i[2,1] <- Lam_1iM[2,m];
  Lambda_M1i[3,1] <- Lam_1iM[3,m];
  Lambda_M1i[4,2] <- Lam_1iM[4,m] ;
  Lambda_M1i[5,2] <- Lam_1iM[5,m] ;
  Lambda_M1i[6,3] <- Lam_1iM[6,m] ;
  Lambda_M1i[7,3] <- Lam_1iM[7,m] ;
  
  Lambda_M2[1,1] <- Lam_2M[1,m]; #eta1
  Lambda_M2[2,1] <- Lam_2M[2,m];
  Lambda_M2[3,1] <- Lam_2M[3,m];
  Lambda_M2[4,2] <- Lam_2M[4,m] ;
  Lambda_M2[5,2] <- Lam_2M[5,m] ;
  Lambda_M2[6,3] <- Lam_2M[6,m] ;
  Lambda_M2[7,3] <- Lam_2M[7,m] ;
  
  Lambda_C_eta <- as.matrix(Lambda_C[1:P,1:P1]);
  Lambda_C_xi <- Lambda_C[1:P,1:P2];
  Lambda_F1i_eta <- as.matrix(Lambda_F1i[1:P,1:P1]);
  Lambda_F1i_xi <- Lambda_F1i[1:P,(P1+1):(P1+P2)];
  Lambda_F2_eta <- as.matrix(Lambda_F2[1:P,1:P1]);
  Lambda_F2_xi <- Lambda_F2[1:P,(P1+1):(P1+P2)];
  Lambda_M1i_eta <- as.matrix(Lambda_M1i[1:P,1:P1]);
  Lambda_M1i_xi <- Lambda_M1i[1:P,(P1+1):(P1+P2)];
  Lambda_M2_eta <- as.matrix(Lambda_M2[1:P,1:P1]);
  Lambda_M2_xi <- Lambda_M2[1:P,(P1+1):(P1+P2)];
  #############################################################################################################################
  CovFF1 <- Lambda_C_eta %*% diag(P1) %*% Psi_idc[, m] %*% t(Lambda_C_eta %*% diag(P1)) + 
    Lambda_F2_eta %*% Pi2F[[m]] %*% (Psi_2dtF[, m]) %*% t(Lambda_F2_eta %*% (Pi2F[[m]])) + 
    Lambda_F1i_eta %*% Pi1F[[m]] %*% (Psi_1idtF[, m]) %*% t(Lambda_F1i_eta %*% Pi1F[[m]]) + 
    diag(Psi_2F[, m] + Psi_1F[, m] + Psi_c[, m])
  
  
  
  sumPsi_F[m, ] <- Psi_1F[, m]
  CovMM1 <- Lambda_C_eta %*% diag(P1) %*% (Psi_idc[, m]) %*% t(Lambda_C_eta %*% diag(P1)) + 
    Lambda_M2_eta %*% Pi2M[[m]] %*% (Psi_2dtM[, m]) %*% t(Lambda_M2_eta %*% Pi2M[[m]]) + 
    Lambda_M1i_eta %*% Pi1M[[m]] %*% (Psi_1idtM[, m]) %*% t(Lambda_M1i_eta %*% Pi1M[[m]]) + 
    diag(Psi_2M[, m] + Psi_1M[, m] + Psi_c[, m])
  
  sumPsi_M[m, ] <- Psi_1M[, m]
  pc <- pcE <- matrix(0, P, numCC)
  for (cc in 1:numCC) {
    pc[, cc] <- muC[, m] + Lambda_C_eta %*% (0%*%c(0,0) %*% 
                                               XiC[cc, m, ]) + (Lambda_C_xi) %*% XiC[cc, m, ]
    pcE[, cc] <- muC[, m] + Lambda_C_eta %*% (0%*%c(0,0) %*% 
                                                XiC[cc, m, ]) + (Lambda_C_xi) %*% XiC[cc, m, ] + EphatC[cc, m, ]
    
  }
  for (i in 1:numFF)
  {
    p2F <- as.vector(muF[, m]) + Lambda_F2_eta %*% 
      (Pi2F[[m]] %*% Gammas_F2 %*% XiF2[i, m, ]) + matrix(Lambda_F2_xi, nrow=P) %*% XiF2[i, m, ]  #average
    y_io_p2F <- as.vector(muF[, m]) + Lambda_F2_eta %*% 
      (Pi2F[[m]] %*% Gammas_F2 %*% XiF2[i, m, ] + (DthatF2[i, m, ])) + 
      (Lambda_F2_xi) %*% XiF2[i, m, ] + 
      Ephat2F[i, m, ]  # with epsilon error
    
    
    for (o in 1:data$Nf[i])
    {
      # number of observations per individuals
      interF <- as.vector(pc[, data$z1c[kkF[i] + o]])
      interFE <- as.vector(pcE[, data$z1c[kkF[i] + o]])
      p3F <- Lambda_F1i_eta %*% Pi1F[[m]] %*% Gammas_F1i %*% XiF1[[m]][kkF[i] + o, ] + 
        (Lambda_F1i_xi) %*% XiF1[[m]][kkF[i] + o, ]
      
      y_io_p3F <- Lambda_F1i_eta %*% (Pi1F[[m]] %*% Gammas_F1i %*% XiF1[[m]][kkF[i] + o, ] + 
                                        DthatF1[[m]][kkF[i] + o, ] + bf[, m] %*% FixF[kkF[i] + o, , m]) + 
        (Lambda_F1i_xi) %*% XiF1[[m]][kkF[i] + o, ] + Ephat1F[kkF[i] + o, m, ]
      
      EYF <- as.vector(interF + p2F + p3F)
      y_ioF <- as.vector(interFE + y_io_p2F + y_io_p3F)
      # yF_io[kkF[i]+o,,m] <- y_ioF
      store_yF_io[kkF[i] + o, ] <- store_yF_io[kkF[i] + o, ] + y_ioF/width
      mu_giFF[kkF[i] + o, , m] <- EYF
      mu_gi_hat_F[kkF[i] + o, ] <- mu_gi_hat_F[kkF[i] + o, ] + EYF/width
      CovF <- CovF + (CovFF1 + EYF %*% t(EYF))/width  # this corresponds to the 1st term of Sum_{gi} hat or 1/S*sum[sum_gi+mu_gi*t(mu_gi)]; used only for continuous variables
      
      # dichotomize ordered categorical variables
      t1 <- t2 <- t3 <- t4 <- t5 <- NULL
      for (n in 1:Ps3)
      {
        # if(y_io[n]<=thd[n,2])
        # {cat3_z[kk[i]+o,(n*3-2):(n*3),m]<-c(1,0,0)} else if
        # (y_io[n]>thd[n,2] & y_io[n]<=thd[n,3]) {
        # \tcat3_z[kk[i]+o,(n*3-2):(n*3),m] <-c(0,1,0)} else if
        # (y_io[n]>thd[n,3])
        # {cat3_z[kk[i]+o,(n*3-2):(n*3),m]<-c(0,0,1)}
        t3 <- (pnorm(((thd3[n, 4] - EYF[n])/sumPsi_F[m, n]), 0, 1) - 
                 pnorm(((thd3[n, 3] - EYF[n])/sumPsi_F[m, n]), 0, 1))
        t2 <- (pnorm(((thd3[n, 3] - EYF[n])/sumPsi_F[m, n]), 0, 1) - 
                 pnorm(((thd3[n, 2] - EYF[n])/sumPsi_F[m, n]), 0, 1))
        t1 <- (pnorm(((thd3[n, 2] - EYF[n])/sumPsi_F[m, n]), 0, 1) - 
                 pnorm(((thd3[n, 1] - EYF[n])/sumPsi_F[m, n]), 0, 1))
        res_3catFF[kkF[i] + o, (n * 3 - 2):(n * 3)] <- res_3catFF[kkF[i] + 
                                                                    o, (n * 3 - 2):(n * 3)] + (c(t1, t2, t3)/width)
        # mu_star_3catF[kkF[i]+o,(n*3-2):(n*3),m] <- c(t1,t2,t3)
      }
      t1 <- t2 <- t3 <- t4 <- t5 <- NULL
      for (n in 1:Ps4)
      {
        # if(y_io[n]<=thd[n,2])
        # {cat3_z[kk[i]+o,(n*3-2):(n*3),m]<-c(1,0,0)} else if
        # (y_io[n]>thd[n,2] & y_io[n]<=thd[n,3]) {
        # \tcat3_z[kk[i]+o,(n*3-2):(n*3),m] <-c(0,1,0)} else if
        # (y_io[n]>thd[n,3])
        # {cat3_z[kk[i]+o,(n*3-2):(n*3),m]<-c(0,0,1)}
        t4 <- (pnorm(((thd4[n, 5] - EYF[Ps3 + Pd + n])/sumPsi_F[m, 
                                                                (Ps3 + Pd + n)]), 0, 1) - pnorm(((thd4[n, 4] - EYF[Ps3 + 
                                                                                                                     Pd + n])/sumPsi_F[m, (Ps3 + Pd + n)]), 0, 1))
        t3 <- (pnorm(((thd4[n, 4] - EYF[Ps3 + Pd + n])/sumPsi_F[m, 
                                                                (Ps3 + Pd + n)]), 0, 1) - pnorm(((thd4[n, 3] - EYF[Ps3 + 
                                                                                                                     Pd + n])/sumPsi_F[m, (Ps3 + Pd + n)]), 0, 1))
        t2 <- (pnorm(((thd4[n, 3] - EYF[Ps3 + Pd + n])/sumPsi_F[m, 
                                                                (Ps3 + Pd + n)]), 0, 1) - pnorm(((thd4[n, 2] - EYF[Ps3 + 
                                                                                                                     Pd + n])/sumPsi_F[m, (Ps3 + Pd + n)]), 0, 1))
        t1 <- (pnorm(((thd4[n, 2] - EYF[Ps3 + Pd + n])/sumPsi_F[m, 
                                                                (Ps3 + Pd + n)]), 0, 1) - pnorm(((thd4[n, 1] - EYF[Ps3 + 
                                                                                                                     Pd + n])/sumPsi_F[m, (Ps3 + Pd + n)]), 0, 1))
        res_4catFF[kkF[i] + o, (n * 4 - 3):(n * 4)] <- res_4catFF[kkF[i] + 
                                                                    o, (n * 4 - 3):(n * 4)] + (c(t1, t2, t3, t4)/width)
        # mu_star_4catF[kkF[i]+o,(n*4-3):(n*4),m] <-
        # c(t1,t2,t3,t4)
      }
      t1 <- t2 <- t3 <- t4 <- t5 <- NULL
      for (n in 1:Ps5)
      {
        # if(y_io[n]<=thd[n,2])
        # {cat3_z[kk[i]+o,(n*3-2):(n*3),m]<-c(1,0,0)} else if
        # (y_io[n]>thd[n,2] & y_io[n]<=thd[n,3]) {
        # \tcat3_z[kk[i]+o,(n*3-2):(n*3),m] <-c(0,1,0)} else if
        # (y_io[n]>thd[n,3])
        # {cat3_z[kk[i]+o,(n*3-2):(n*3),m]<-c(0,0,1)}
        t5 <- (pnorm(((thd5[n, 6] - EYF[Ps3 + Pd + Ps4 + n])/sumPsi_F[m, 
                                                                      (Ps3 + Pd + Ps4 + n)]), 0, 1) - pnorm(((thd5[n, 5] - EYF[Ps3 + 
                                                                                                                                 Pd + Ps4 + n])/sumPsi_F[m, (Ps3 + Pd + Ps4 + n)]), 0, 1))
        t4 <- (pnorm(((thd5[n, 5] - EYF[Ps3 + Pd + Ps4 + n])/sumPsi_F[m, 
                                                                      (Ps3 + Pd + Ps4 + n)]), 0, 1) - pnorm(((thd5[n, 4] - EYF[Ps3 + 
                                                                                                                                 Pd + Ps4 + n])/sumPsi_F[m, (Ps3 + Pd + Ps4 + n)]), 0, 1))
        t3 <- (pnorm(((thd5[n, 4] - EYF[Ps3 + Pd + Ps4 + n])/sumPsi_F[m, 
                                                                      (Ps3 + Pd + Ps4 + n)]), 0, 1) - pnorm(((thd5[n, 3] - EYF[Ps3 + 
                                                                                                                                 Pd + Ps4 + n])/sumPsi_F[m, (Ps3 + Pd + Ps4 + n)]), 0, 1))
        t2 <- (pnorm(((thd5[n, 3] - EYF[Ps3 + Pd + Ps4 + n])/sumPsi_F[m, 
                                                                      (Ps3 + Pd + Ps4 + n)]), 0, 1) - pnorm(((thd5[n, 2] - EYF[Ps3 + 
                                                                                                                                 Pd + Ps4 + n])/sumPsi_F[m, (Ps3 + Pd + Ps4 + n)]), 0, 1))
        t1 <- (pnorm(((thd5[n, 2] - EYF[Ps3 + Pd + Ps4 + n])/sumPsi_F[m, 
                                                                      (Ps3 + Pd + Ps4 + n)]), 0, 1) - pnorm(((thd5[n, 1] - EYF[Ps3 + 
                                                                                                                                 Pd + Ps4 + n])/sumPsi_F[m, (Ps3 + Pd + Ps4 + n)]), 0, 1))
        res_5catFF[kkF[i] + o, (n * 5 - 4):(n * 5)] <- res_5catFF[kkF[i] + 
                                                                    o, (n * 5 - 4):(n * 5)] + (c(t1, t2, t3, t4, t5)/width)
        # mu_star_5catF[kkF[i]+o,(n*5-4):(n*5),m] <-
        # c(t1,t2,t3,t4,t5)
      }
      # mu_starF[kkF[i]+o,,m] <- \tc(mu_star_3catF[kkF[i]+o,,m],
      # mu_star_2catF[kkF[i]+o,,m], mu_star_4catF[kkF[i]+o,,m],
      # mu_star_5catF[kkF[i]+o,,m])
    }
  }
  for (i in 1:numMM)
  {
    p2M <- as.vector(muM[, m]) + Lambda_M2_eta %*% (Pi2M[[m]] %*% Gammas_M2 %*% 
                                                      XiM2[i, m, ]) + matrix(Lambda_M2_xi, nrow=P) %*% XiM2[i, m, ]
    y_io_p2M <- as.vector(muM[, m]) + 
      Lambda_M2_eta %*% (Pi2M[[m]] %*% Gammas_M2 %*% XiM2[i, m, ] + (DthatM2[i, m, ])) + 
      (Lambda_M2_xi) %*% XiM2[i, m, ] + 
      Ephat2M[i, m, ]  #includes measurement error
    
    
    for (o in 1:data$Nm[i])
    {
      interM <- as.vector(pc[, data$z2c[kkM[i] + o]])
      interME <- as.vector(pcE[, data$z2c[kkM[i] + o]])
      p3M <- Lambda_M1i_eta %*% Pi1M[[m]] %*% Gammas_M1i %*% XiM1[[m]][kkM[i] + o, ] + 
        (Lambda_M1i_xi) %*% XiM1[[m]][kkM[i] + o, ]
      
      
      y_io_p3M <- Lambda_M1i_eta %*% (Pi1M[[m]] %*% Gammas_M1i %*% XiM1[[m]][kkM[i] + o, ] + 
                                        bm[, m] %*% FixM[kkM[i] + o, , m] + DthatM1[[m]][kkM[i] + o, ]) + 
        (Lambda_M1i_xi) %*% XiM1[[m]][kkM[i] + o, ] + Ephat1M[kkM[i] + o, m, ]
      EYM <- as.vector(interM + p2M + p3M)
      y_ioM <- as.vector(interME + y_io_p2M + y_io_p3M)
      # yM_io[kkM[i]+o,,m] <- y_ioM
      store_yM_io[kkM[i] + o, ] <- store_yM_io[kkM[i] + o, ] + y_ioM/width
      mu_giMM[kkM[i] + o, , m] <- EYM
      mu_gi_hat_M[kkM[i] + o, ] <- mu_gi_hat_M[kkM[i] + o, ] + EYM/width
      CovM <- CovM + diag(CovMM1 + EYM %*% t(EYM))/width
      
      t1 <- t2 <- t3 <- t4 <- t5 <- NULL
      for (n in 1:Ps3)
      {
        # if(y_io[n]<=thd[n,2])
        # {cat3_z[kk[i]+o,(n*3-2):(n*3),m]<-c(1,0,0)} else if
        # (y_io[n]>thd[n,2] & y_io[n]<=thd[n,3]) {
        # \tcat3_z[kk[i]+o,(n*3-2):(n*3),m] <-c(0,1,0)} else if
        # (y_io[n]>thd[n,3])
        # {cat3_z[kk[i]+o,(n*3-2):(n*3),m]<-c(0,0,1)}
        t3 <- (pnorm(((thd3[n, 4] - EYM[n])/sumPsi_M[m, n]), 0, 1) - 
                 pnorm(((thd3[n, 3] - EYM[n])/sumPsi_M[m, n]), 0, 1))
        t2 <- (pnorm(((thd3[n, 3] - EYM[n])/sumPsi_M[m, n]), 0, 1) - 
                 pnorm(((thd3[n, 2] - EYM[n])/sumPsi_M[m, n]), 0, 1))
        t1 <- (pnorm(((thd3[n, 2] - EYM[n])/sumPsi_M[m, n]), 0, 1) - 
                 pnorm(((thd3[n, 1] - EYM[n])/sumPsi_M[m, n]), 0, 1))
        res_3catMM[kkM[i] + o, (n * 3 - 2):(n * 3)] <- res_3catMM[kkM[i] + 
                                                                    o, (n * 3 - 2):(n * 3)] + (c(t1, t2, t3)/width)
        # mu_star_3catM[kkM[i]+o,(n*3-2):(n*3),m]
      }
      t1 <- t2 <- t3 <- t4 <- t5 <- NULL
      for (n in 1:Ps4)
      {
        # if(y_io[n]<=thd[n,2])
        # {cat3_z[kk[i]+o,(n*3-2):(n*3),m]<-c(1,0,0)} else if
        # (y_io[n]>thd[n,2] & y_io[n]<=thd[n,3]) {
        # \tcat3_z[kk[i]+o,(n*3-2):(n*3),m] <-c(0,1,0)} else if
        # (y_io[n]>thd[n,3])
        # {cat3_z[kk[i]+o,(n*3-2):(n*3),m]<-c(0,0,1)}
        t4 <- (pnorm(((thd4[n, 5] - EYM[Ps3 + Pd + n])/sumPsi_M[m, 
                                                                (Ps3 + Pd + n)]), 0, 1) - pnorm(((thd4[n, 4] - EYM[Ps3 + 
                                                                                                                     Pd + n])/sumPsi_M[m, (Ps3 + Pd + n)]), 0, 1))
        t3 <- (pnorm(((thd4[n, 4] - EYM[Ps3 + Pd + n])/sumPsi_M[m, 
                                                                (Ps3 + Pd + n)]), 0, 1) - pnorm(((thd4[n, 3] - EYM[Ps3 + 
                                                                                                                     Pd + n])/sumPsi_M[m, (Ps3 + Pd + n)]), 0, 1))
        t2 <- (pnorm(((thd4[n, 3] - EYM[Ps3 + Pd + n])/sumPsi_M[m, 
                                                                (Ps3 + Pd + n)]), 0, 1) - pnorm(((thd4[n, 2] - EYM[Ps3 + 
                                                                                                                     Pd + n])/sumPsi_M[m, (Ps3 + Pd + n)]), 0, 1))
        t1 <- (pnorm(((thd4[n, 2] - EYM[Ps3 + Pd + n])/sumPsi_M[m, 
                                                                (Ps3 + Pd + n)]), 0, 1) - pnorm(((thd4[n, 1] - EYM[Ps3 + 
                                                                                                                     Pd + n])/sumPsi_M[m, (Ps3 + Pd + n)]), 0, 1))
        res_4catMM[kkM[i] + o, (n * 4 - 3):(n * 4)] <- res_4catMM[kkM[i] + 
                                                                    o, (n * 4 - 3):(n * 4)] + (c(t1, t2, t3, t4)/width)
        # mu_star_4catM[kkM[i]+o,(n*4-3):(n*4),m] <-
        # c(t1,t2,t3,t4)
      }
      t1 <- t2 <- t3 <- t4 <- t5 <- NULL
      for (n in 1:Ps5)
      {
        # if(y_io[n]<=thd[n,2])
        # {cat3_z[kk[i]+o,(n*3-2):(n*3),m]<-c(1,0,0)} else if
        # (y_io[n]>thd[n,2] & y_io[n]<=thd[n,3]) {
        # \tcat3_z[kk[i]+o,(n*3-2):(n*3),m] <-c(0,1,0)} else if
        # (y_io[n]>thd[n,3])
        # {cat3_z[kk[i]+o,(n*3-2):(n*3),m]<-c(0,0,1)}
        t5 <- (pnorm(((thd5[n, 6] - EYM[Ps3 + Pd + Ps4 + n])/sumPsi_M[m, 
                                                                      (Ps3 + Pd + Ps4 + n)]), 0, 1) - pnorm(((thd5[n, 5] - EYM[Ps3 + 
                                                                                                                                 Pd + Ps4 + n])/sumPsi_M[m, (Ps3 + Pd + Ps4 + n)]), 0, 1))
        t4 <- (pnorm(((thd5[n, 5] - EYM[Ps3 + Pd + Ps4 + n])/sumPsi_M[m, 
                                                                      (Ps3 + Pd + Ps4 + n)]), 0, 1) - pnorm(((thd5[n, 4] - EYM[Ps3 + 
                                                                                                                                 Pd + Ps4 + n])/sumPsi_M[m, (Ps3 + Pd + Ps4 + n)]), 0, 1))
        t3 <- (pnorm(((thd5[n, 4] - EYM[Ps3 + Pd + Ps4 + n])/sumPsi_M[m, 
                                                                      (Ps3 + Pd + Ps4 + n)]), 0, 1) - pnorm(((thd5[n, 3] - EYM[Ps3 + 
                                                                                                                                 Pd + Ps4 + n])/sumPsi_M[m, (Ps3 + Pd + Ps4 + n)]), 0, 1))
        t2 <- (pnorm(((thd5[n, 3] - EYM[Ps3 + Pd + Ps4 + n])/sumPsi_M[m, 
                                                                      (Ps3 + Pd + Ps4 + n)]), 0, 1) - pnorm(((thd5[n, 2] - EYM[Ps3 + 
                                                                                                                                 Pd + Ps4 + n])/sumPsi_M[m, (Ps3 + Pd + Ps4 + n)]), 0, 1))
        t1 <- (pnorm(((thd5[n, 2] - EYM[Ps3 + Pd + Ps4 + n])/sumPsi_M[m, 
                                                                      (Ps3 + Pd + Ps4 + n)]), 0, 1) - pnorm(((thd5[n, 1] - EYM[Ps3 + 
                                                                                                                                 Pd + Ps4 + n])/sumPsi_M[m, (Ps3 + Pd + Ps4 + n)]), 0, 1))
        res_5catMM[kkM[i] + o, (n * 5 - 4):(n * 5)] <- res_5catMM[kkM[i] + 
                                                                    o, (n * 5 - 4):(n * 5)] + (c(t1, t2, t3, t4, t5)/width)
        # mu_star_5catM[kkM[i]+o,(n*5-4):(n*5),m] <-
        # c(t1,t2,t3,t4,t5)
      }
      # mu_starM[kkM[i]+o,,m] <- \tc(mu_star_3catM[kkM[i]+o,,m],
      # mu_star_2catM[kkM[i]+o,,m], mu_star_4catM[kkM[i]+o,,m],
      # mu_star_5catM[kkM[i]+o,,m])
    }
  }
} # end of m


for (i in 1:nObsFF)
{
  for (n in 1:Ps3)
  {
    if (store_yF_io[i, n] <= thd3[n, 2])
    {
      cat3_zFF[i, (n * 3 - 2):(n * 3)] <- c(1, 0, 0)
    } else if (store_yF_io[i, n] > thd3[n, 2] & store_yF_io[i, n] <= 
               thd3[n, 3])
    {
      cat3_zFF[i, (n * 3 - 2):(n * 3)] <- c(0, 1, 0)
    } else if (store_yF_io[i, n] > thd3[n, 3])
    {
      cat3_zFF[i, (n * 3 - 2):(n * 3)] <- c(0, 0, 1)
    }
  }
  for (n in 1:Ps4)
  {
    if (store_yF_io[i, (Ps3 + Pd + n)] <= thd4[n, 2])
    {
      cat4_zFF[i, (n * 4 - 3):(n * 4)] <- c(1, 0, 0, 0)
    } else if (store_yF_io[i, (Ps3 + Pd + n)] > thd4[n, 2] & store_yF_io[i, 
                                                                         (Ps3 + Pd + n)] <= thd4[n, 3])
    {
      cat4_zFF[i, (n * 4 - 3):(n * 4)] <- c(0, 1, 0, 0)
    } else if (store_yF_io[i, (Ps3 + Pd + n)] > thd4[n, 3] & store_yF_io[i, 
                                                                         (Ps3 + Pd + n)] <= thd4[n, 4])
    {
      cat4_zFF[i, (n * 4 - 3):(n * 4)] <- c(0, 0, 1, 0)
    } else if (store_yF_io[i, (Ps3 + Pd + n)] > thd4[n, 4])
    {
      cat4_zFF[i, (n * 4 - 3):(n * 4)] <- c(0, 0, 0, 1)
    }
  }
  for (n in 1:Ps5)
  {
    if (store_yF_io[i, (Ps4 + Ps3 + Pd + n)] <= thd5[n, 2])
    {
      cat5_zFF[i, (n * 5 - 4):(n * 5)] <- c(1, 0, 0, 0, 0)
    } else if (store_yF_io[i, (Ps4 + Ps3 + Pd + n)] > thd5[n, 2] & store_yF_io[i, 
                                                                               (Ps4 + Ps3 + Pd + n)] <= thd5[n, 3])
    {
      cat5_zFF[i, (n * 5 - 4):(n * 5)] <- c(0, 1, 0, 0, 0)
    } else if (store_yF_io[i, (Ps4 + Ps3 + Pd + n)] > thd5[n, 3] & store_yF_io[i, 
                                                                               (Ps4 + Ps3 + Pd + n)] <= thd5[n, 4])
    {
      cat5_zFF[i, (n * 5 - 4):(n * 5)] <- c(0, 0, 1, 0, 0)
    } else if (store_yF_io[i, (Ps4 + Ps3 + Pd + n)] > thd5[n, 4] & store_yF_io[i, 
                                                                               (Ps4 + Ps3 + Pd + n)] <= thd5[n, 5])
    {
      cat5_zFF[i, (n * 5 - 4):(n * 5)] <- c(0, 0, 0, 1, 0)
    } else if (store_yF_io[i, (Ps4 + Ps3 + Pd + n)] > thd5[n, 5])
    {
      cat5_zFF[i, (n * 5 - 4):(n * 5)] <- c(0, 0, 0, 0, 1)
    }
  }
}
EzrepFF <- cbind(res_3catFF, res_4catFF, res_5catFF)
z_starFF <- cbind(cat3_zFF, cat4_zFF, cat5_zFF)

TR_catFF <- sum(diag(t(EzrepFF) %*% (1 - EzrepFF)))  #for continuous variables need to add CovF +

LMFF <- TR_catFF + 0.5 * sum(diag(t(EzrepFF - z_starFF) %*% (EzrepFF - z_starFF)))
LMFF8 <- TR_catFF + 0.8 * sum(diag(t(EzrepFF - z_starFF) %*% (EzrepFF - z_starFF)))

for (i in 1:nObsMM)
{
  for (n in 1:Ps3)
  {
    if (store_yM_io[i, n] <= thd3[n, 2])
    {
      cat3_zMM[i, (n * 3 - 2):(n * 3)] <- c(1, 0, 0)
    } else if (store_yM_io[i, n] > thd3[n, 2] & store_yM_io[i, n] <= 
               thd3[n, 3])
    {
      cat3_zMM[i, (n * 3 - 2):(n * 3)] <- c(0, 1, 0)
    } else if (store_yM_io[i, n] > thd3[n, 3])
    {
      cat3_zMM[i, (n * 3 - 2):(n * 3)] <- c(0, 0, 1)
    }
  }
  for (n in 1:Ps4)
  {
    if (store_yM_io[i, (Ps3 + Pd + n)] <= thd4[n, 2])
    {
      cat4_zMM[i, (n * 4 - 3):(n * 4)] <- c(1, 0, 0, 0)
    } else if (store_yM_io[i, (Ps3 + Pd + n)] > thd4[n, 2] & store_yM_io[i, 
                                                                         (Ps3 + Pd + n)] <= thd4[n, 3])
    {
      cat4_zMM[i, (n * 4 - 3):(n * 4)] <- c(0, 1, 0, 0)
    } else if (store_yM_io[i, (Ps3 + Pd + n)] > thd4[n, 3] & store_yM_io[i, 
                                                                         (Ps3 + Pd + n)] <= thd4[n, 4])
    {
      cat4_zMM[i, (n * 4 - 3):(n * 4)] <- c(0, 0, 1, 0)
    } else if (store_yM_io[i, (Ps3 + Pd + n)] > thd4[n, 4])
    {
      cat4_zMM[i, (n * 4 - 3):(n * 4)] <- c(0, 0, 0, 1)
    }
  }
  for (n in 1:Ps5)
  {
    if (store_yM_io[i, (Ps4 + Ps3 + Pd + n)] <= thd5[n, 2])
    {
      cat5_zMM[i, (n * 5 - 4):(n * 5)] <- c(1, 0, 0, 0, 0)
    } else if (store_yM_io[i, (Ps4 + Ps3 + Pd + n)] > thd5[n, 2] & store_yM_io[i, 
                                                                               (Ps4 + Ps3 + Pd + n)] <= thd5[n, 3])
    {
      cat5_zMM[i, (n * 5 - 4):(n * 5)] <- c(0, 1, 0, 0, 0)
    } else if (store_yM_io[i, (Ps4 + Ps3 + Pd + n)] > thd5[n, 3] & store_yM_io[i, 
                                                                               (Ps4 + Ps3 + Pd + n)] <= thd5[n, 4])
    {
      cat5_zMM[i, (n * 5 - 4):(n * 5)] <- c(0, 0, 1, 0, 0)
    } else if (store_yM_io[i, (Ps4 + Ps3 + Pd + n)] > thd5[n, 4] & store_yM_io[i, 
                                                                               (Ps4 + Ps3 + Pd + n)] <= thd5[n, 5])
    {
      cat5_zMM[i, (n * 5 - 4):(n * 5)] <- c(0, 0, 0, 1, 0)
    } else if (store_yM_io[i, (Ps4 + Ps3 + Pd + n)] > thd5[n, 5])
    {
      cat5_zMM[i, (n * 5 - 4):(n * 5)] <- c(0, 0, 0, 0, 1)
    }
  }
}
EzrepMM <- cbind(res_3catMM, res_4catMM, res_5catMM)
z_starMM <- cbind(cat3_zMM, cat4_zMM, cat5_zMM)

TR_catMM <- sum(diag(t(EzrepMM) %*% (1 - EzrepMM)))


LMMM <- TR_catMM + 0.5 * sum(diag(t(EzrepMM - z_starMM) %*% (EzrepMM - z_starMM)))
LMMM8 <- TR_catMM + 0.8 * sum(diag(t(EzrepMM - z_starMM) %*% (EzrepMM - z_starMM)))

L_nu <- round(LMMM + LMFF, 2)
L_nu8 <- round(LMMM8 + LMFF8,2)
